<?php

require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$shaun = new Animal('shaun');
echo "Name : ".$shaun->nama."<br>";
echo "legs : ".$shaun->legs."<br>";
echo "cold blooded : ".$shaun->cold_blooded."<br><br>";

$buduk = new Frog('buduk');
echo "Name : ".$buduk->nama."<br>";
echo "legs : ".$buduk->legs."<br>";
echo "cold blooded : ".$buduk->cold_blooded."<br>";
echo $buduk->Jump("Hop Hop")."<br><br>";

$kera_sakti = new Ape('kera sakti');
echo "Name : ".$kera_sakti->nama."<br>";
echo "legs : ".$kera_sakti->legs."<br>";
echo "cold blooded : ".$kera_sakti->cold_blooded."<br>";
echo $kera_sakti->Yell("Auooo");
?>
